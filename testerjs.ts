namespace TesterJs {
    declare let jQuery: any;
    declare let Materialize: any;
    declare let localStorage: any;
    let connections = [], connectionsData = [], objects = [], runTime;
    class Link {
        private static _instance: Link;
        private start;
        private end;

        public set(point) {
            if (this.start) {
                this.setEnd(point);
            } else {
                this.setStart(point);
            }
        }

        setSelected(elm) {
            elm.find('.card').addClass('yellow');
            elm.find('.card').removeClass('blue-grey');
        }

        setUnselected(elm) {
            elm.find('.card').addClass('blue-grey');
            elm.find('.card').removeClass('yellow');
        }

        setStart(point) {
            this.start = jQuery(point).attr('id');
            this.setSelected(jQuery(point));
        }

        setEnd(point) {
            this.end = jQuery(point).attr('id');
            if (this.start === this.end) {
                this.setUnselected(jQuery('#' + this.start));
            } else {
                connections.push(new jQuery.connect('#' + this.start, '#' + this.end));
                connectionsData.push({
                    start: this.start,
                    end: this.end,
                });
                this.setUnselected(jQuery('#' + this.start));
            }
            this.start = null;
            this.end = null;
            console.log(connectionsData);
        }

        public static getInstance() {
            return this._instance || (this._instance = new this());
        }
    }
    class TesterObject {
        elm;
        id;
        type;
        position;

        constructor(id, type, position?) {
            let me = this;
            this.type = type;
            this.elm = jQuery('.' + type + '-element').first().clone().appendTo('#container');
            this.elm.removeClass('hide');
            this.elm.removeClass('sample-element');
            this.elm.addClass('element');
            if (position) {
                this.elm.offset(position);
            }
            this.id = id;
            this.elm.attr('id', 'element-' + this.id);
            //this.elm.find('.card-title').html(this.elm.find('.card-title').html() + '&nbsp;|&nbsp;element-' + this.id);
            this.elm.draggable({
                drag: function () {
                    let item = this;
                    connections.forEach(function (connection) {
                        if (connection.elem1[0] === item || connection.elem2[0] === item) {
                            connection.calculate();
                        }
                    });
                    me.position = me.elm.position();
                }
            });
            this.elm.find('.grab-left, .grab-right').click(function () {
                let link = Link.getInstance();
                link.set(me.elm);
            });

            switch (type) {
                case 'start':
                    me.elm.find('.run').click(function () {
                        runTime = new Date().getTime();
                        me.run();
                    });
                    break;
            }
            this.elm.click(function () {
                me.edit();
            });
        }

        edit() {
            if (this.elm.find('.side-nav').length) {
                jQuery('.button-sidenav').sideNav('show');
                jQuery('#slide-out').html(this.elm.find('.side-nav').html());
                this.elm.find('.card').addClass('yellow');
                this.elm.find('.card').removeClass('blue-grey');
            }
        }

        run(input?) {
            let me = this;
            let dataElement = jQuery('#slide-out');
            this.elm.find('.card').removeClass('blue-grey');
            this.elm.find('.card').addClass('yellow');
            switch (this.type) {
                case 'start':
                    me.next();
                    break;
                case 'code':
                    eval(dataElement.find('textarea').val());
                    me.next(dataElement.find('textarea').val());
                    break;
                case 'ajax':
                    jQuery.ajax({
                        method: dataElement.find('.method').val(),
                        url: dataElement.find('.url').val(),
                        data: JSON.parse(dataElement.find('.data').val())
                    })
                        .done(function (msg) {
                            me.next(msg);
                        });
                    break;
                case 'timeout':
                    setTimeout(function () {
                        me.next(dataElement.find('input').val());
                    }, dataElement.find('input').val());
                    break;
                case 'console':
                    if (typeof input === 'object') {
                        dataElement.find('.console-output').html(JSON.stringify(input));
                    } else {
                        dataElement.find('.console-output').html(input);
                    }
                    me.next();
                    break;
            }

        }

        next(output?) {
            let me = this;
            me.elm.find('.card').removeClass('yellow');
            me.elm.find('.card').addClass('green');
            let tmp2 = 0;
            connectionsData.forEach(function (a) {
                if (a.start === 'element-' + me.id) {
                    let tmp = 0;
                    tmp2++;
                    objects.forEach(function (object) {
                        if ('element-' + object.id === a.end) {
                            tmp++;
                            return object.run(output);
                        }
                    });
                    console.log(tmp);
                    if (!tmp) {
                        me.finish();
                    }
                }
            });
            if (!tmp2) {
                me.finish();
            }
        }

        finish() {
            jQuery('.run').find('.btn-floating').removeClass('grey');
            jQuery('.run').find('.btn-floating').addClass('green');
            jQuery('.run').find('.btn-floating>i').html('play_arrow');
            let tmp = new Date().getTime();
            Materialize.toast('Finished in ' + (tmp - runTime) + ' milliseconds', 4000);
        }
    }
    class Storage {
        constructor() {
            this.restore();
        }

        save() {
            localStorage.setItem('tester-js.storage', JSON.stringify({
                'connections': connections,
                'connectionsData': connectionsData,
                'objects': objects
            }));
        }

        restore() {
            let savedData = JSON.parse(localStorage.getItem('tester-js.storage'));
            console.log(savedData);
            if (!savedData || !savedData.connections) {
                connections = [];
            }
            if (!savedData || !savedData.connectionsData) {
                connectionsData = [];
            }
            if (savedData && savedData.objects) {
                savedData.objects.forEach(function (object) {
                    objects.push(new TesterObject(objects.length, object.type, object.position));
                });
            }
        }
    }
    export class init {
        constructor(selector, inputData) {
            let start, end, me = this;
            let storage = new Storage();
            //objects.push(new TesterObject(objects.length, 'start'));
            jQuery('.addElement').click(function () {
                objects.push(new TesterObject(objects.length, jQuery(this).attr('data-type')));
            });
            jQuery('.removeAll').click(function () {
                connections = [], connectionsData = [], objects = [];
                jQuery('.element').remove();
                jQuery('.connector').remove();
            });
            jQuery('.save').click(function () {
                storage.save();
            });
            jQuery(".button-sidenav").sideNav({
                menuWidth: 300,
                edge: 'right',
                closeOnClick: true
            });
        }
    }
}