# KlJs

## Easy-to-use Javascript micro-framework for binding

### How to install

```
npm install
```
```
bower install
```

### How to use

Let's imagine you have a DOM like this

```
<a class="testlink" href="">
</a>
```
You will bind your data :

```
Kl.Bind(".testlink", {
    "content": "My Link text",
    "href": "http://mywebsite.com"
});
```
You could also apply style :

```
Kl.Bind(".testlink", {
    "content": "My Link text",
    "href": "http://mywebsite.com"
    "style": {
        "color": "blue",
        "textDecoration": "none",
        "backgroundImage": "url('https://www.google.fr/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png')"
    }
});
```

Now if you have a complex DOM : 

```
<a class="testlink" href="">
    <span class="first-item">
        <ul>
            <li class="withclass"></li>
            <li></li>
        </ul>
    </span>
</a>
```
You will bind your data and your style like that :

```
Kl.Bind(".testlink", {
    "href": "http://mywebsite.com",
    "style": {
        "textAlign":"center"
    },
    "ul>li": {
        "content": "Test",
        "style": {
            "fontSize": "2em"
        }
    },
    "li.withclass": {
        "content":"Test2"
    }
});
```