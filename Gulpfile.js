var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
var rename = require("gulp-rename");
var typescript = require('gulp-typescript');
var watch = require('gulp-watch');


gulp.task('compile:js', function (cb) {
  return gulp.src('testerjs.ts')
    .pipe(typescript({
      "target": "es5"
    }))
    .pipe(gulp.dest('dist'))
});

gulp.task('build:js', function (cb) {
  pump([
      gulp.src('dist/testerjs.js'),
      uglify(),
      rename("testerjs.min.js"),
      gulp.dest('.')
    ],
    cb
  );
});

var browserSync = require('browser-sync');
var proxyMiddleware = require('http-proxy-middleware');
var proxy1 = proxyMiddleware('/api', {
  changeOrigin: true,
  target: 'http://proxy.api.tourism-system.com',
  pathRewrite: {"^/api": "/"}
});

function browserSyncInit(baseDir, files, browser) {
  browser = 'default';
  browserSync.instance = browserSync.init(files, {
    startPath: '/',
    server: {
      baseDir: baseDir,
      middleware: [proxy1]
    },
    browser: browser,
    ghostMode: false
  });
}
gulp.task('watch', function(){
  return gulp.watch('testerjs.ts', ['compile:js']);
});

gulp.task('serve', ['watch'], function (cb) {
  browserSyncInit('.', [
    'dist/testerjs.js',
    'index.html'
  ]);
});